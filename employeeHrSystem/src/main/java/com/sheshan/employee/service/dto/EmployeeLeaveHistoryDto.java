package com.sheshan.employee.service.dto;

public class EmployeeLeaveHistoryDto {
	
	private String fromdate;
	private String todate;
	private double leavecount;
	private String leavetype;
	private String covering;
	private String status;
	private String approved;
	
	public String getFromdate() {
		return fromdate;
	}
	public void setFromdate(String fromdate) {
		this.fromdate = fromdate;
	}
	public String getTodate() {
		return todate;
	}
	public void setTodate(String todate) {
		this.todate = todate;
	}
	public double getLeavecount() {
		return leavecount;
	}
	public void setLeavecount(double leavecount) {
		this.leavecount = leavecount;
	}
	public String getLeavetype() {
		return leavetype;
	}
	public void setLeavetype(String leavetype) {
		this.leavetype = leavetype;
	}
	public String getCovering() {
		return covering;
	}
	public void setCovering(String covering) {
		this.covering = covering;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getApproved() {
		return approved;
	}
	public void setApproved(String approved) {
		this.approved = approved;
	}
	
	
	

}
