package com.sheshan.employee.service.dto;

public class DepartmentDTo {
	
	private int dep_id;
	private String dep_name;
	private int dep_hod;
	private int acting_hod;
	private int [] report_viewer_before;
	private int [] report_viewer;
	private String status;
	
	public int getDep_id() {
		return dep_id;
	}
	public void setDep_id(int dep_id) {
		this.dep_id = dep_id;
	}
	public String getDep_name() {
		return dep_name;
	}
	public void setDep_name(String dep_name) {
		this.dep_name = dep_name;
	}
	public int getDep_hod() {
		return dep_hod;
	}
	public void setDep_hod(int dep_hod) {
		this.dep_hod = dep_hod;
	}
	public int getActing_hod() {
		return acting_hod;
	}
	public void setActing_hod(int acting_hod) {
		this.acting_hod = acting_hod;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int[] getReport_viewer() {
		return report_viewer;
	}
	public void setReport_viewer(int[] report_viewer) {
		this.report_viewer = report_viewer;
	}
	public int[] getReport_viewer_before() {
		return report_viewer_before;
	}
	public void setReport_viewer_before(int[] report_viewer_before) {
		this.report_viewer_before = report_viewer_before;
	}
	
	
}
