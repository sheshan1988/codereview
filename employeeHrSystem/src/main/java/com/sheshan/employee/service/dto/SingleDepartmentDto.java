package com.sheshan.employee.service.dto;

import java.util.ArrayList;

public class SingleDepartmentDto {
	
	private String dep_name;
	private int dep_id;
	private GetAllEmployeeDto dep_hod;
	private String status;
	private GetAllEmployeeDto acting_hod;
	private ArrayList<GetAllEmployeeDto> report_viewer;
	private ArrayList<GetAllEmployeeDto> employees;
	
	public String getDep_name() {
		return dep_name;
	}
	public void setDep_name(String dep_name) {
		this.dep_name = dep_name;
	}
	public int getDep_id() {
		return dep_id;
	}
	public void setDep_id(int dep_id) {
		this.dep_id = dep_id;
	}
	public GetAllEmployeeDto getDep_hod() {
		return dep_hod;
	}
	public void setDep_hod(GetAllEmployeeDto dep_hod) {
		this.dep_hod = dep_hod;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public GetAllEmployeeDto getActing_hod() {
		return acting_hod;
	}
	public void setActing_hod(GetAllEmployeeDto acting_hod) {
		this.acting_hod = acting_hod;
	}
	public ArrayList<GetAllEmployeeDto> getReport_viewer() {
		return report_viewer;
	}
	public void setReport_viewer(ArrayList<GetAllEmployeeDto> report_viewer) {
		this.report_viewer = report_viewer;
	}
	public ArrayList<GetAllEmployeeDto> getEmployees() {
		return employees;
	}
	public void setEmployees(ArrayList<GetAllEmployeeDto> employees) {
		this.employees = employees;
	}
	
}
