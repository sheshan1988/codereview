package com.sheshan.employee.service;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.sheshan.employee.service.dto.EmployeeFullProfileDto;
import com.sheshan.employee.service.dto.EmployeeDto;
import com.sheshan.employee.service.dto.GetAllEmployeeDto;
import com.sheshan.employee.service.logic.EmployeeLogic;




@Path("/employee")
public class EmployeeService {
	
	private final String NO_CONTECT = "No Content";
	private final String EMPLOYEE_ADD_SUCESS_MESSAGE = "{\"success\" : true,\n\"message\" : New employee ";
	private final String EMPLOYEE_ADD_ERROR_MESSAGE = "{\"error\" : true,\n\"message\" : \"System Error\" }";
	private final String EMPLOYEE_UPDATE_ERROR_MESSAGE = "{\"error\" : true,\n\"message\" : \"System Error\" }";

	
	@GET
	@Path("/validate")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMsg(@QueryParam("token") String tokenId) {

		EmployeeLogic employeeLogincHandelr = new EmployeeLogic();
		EmployeeDto employeeDto = employeeLogincHandelr.validateEmployeeLogin(tokenId);
		Gson g = new Gson();
		String response = g.toJson(employeeDto);
		return Response.status(200).entity(response).build();
	}
	
	@GET
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	public Response validateEmployeeLogin(@QueryParam("token") String tokenId) {

		EmployeeLogic employeeLogincHandelr = new EmployeeLogic();
		EmployeeDto employeeDto = employeeLogincHandelr.validateEmployeeLogin(tokenId);
		Gson g = new Gson();
		String response = g.toJson(employeeDto);
		return Response.status(200).entity(response).build();
	}

	@GET
	@Path("/group")
	@Produces(MediaType.APPLICATION_JSON)
	public Response employeesByGroupId(@QueryParam("groupid") String groupid) {

		EmployeeLogic employeeLogincHandelr = new EmployeeLogic();
		ArrayList<EmployeeDto> employeeDtoList = employeeLogincHandelr.getEmployeeListByGroupId(groupid);	
		Gson g = new Gson();
		String response = g.toJson(employeeDtoList);
		return Response.status(200).entity(response).build();
	}
	
	@POST
	@Path("add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addEmployee(EmployeeFullProfileDto employeeAddDto){
		EmployeeLogic employeeLogincHandelr = new EmployeeLogic();
		boolean result = employeeLogincHandelr.addEmployee(employeeAddDto);
		if(result){
			return Response.status(201).entity(EMPLOYEE_ADD_SUCESS_MESSAGE + employeeAddDto.getFullname()+" was added successfully\" }").build();
		}else{
			return Response.status(201).entity(EMPLOYEE_ADD_ERROR_MESSAGE).build();
		}
		
	}
	
	@POST
	@Path("update")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateEmployee(EmployeeFullProfileDto employeeAddDto){
		EmployeeLogic employeeLogincHandelr = new EmployeeLogic();
		boolean result = employeeLogincHandelr.updateEmployee(employeeAddDto);
		if(result){
			String message = "{\"success\" : true,\n\"message\" : "+employeeAddDto.getFullname()+"’s profile updated\"}";
			return Response.status(201).entity(message).build();
			
		}else{
			return Response.status(201).entity(EMPLOYEE_UPDATE_ERROR_MESSAGE).build();
		}
		
	}
	
	
	@GET
	@Path("/get_all")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllEMployees(@DefaultValue("")@QueryParam("name") String name  ,@DefaultValue("0") @QueryParam("limit") int limit) {
		EmployeeLogic employeeLogincHandelr = new EmployeeLogic();
		ArrayList<GetAllEmployeeDto> getAllEmployeeDto= employeeLogincHandelr.getAllEmployees(name, limit);
		if(getAllEmployeeDto==null){
			return Response.status(204).entity(NO_CONTECT).build();
		}
		Gson g = new Gson();
		String response = g.toJson(getAllEmployeeDto);
		return Response.status(200).entity(response).build();
	}
	
	
	@GET
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllEMployeeById(@QueryParam("id") int employeeId) {
		EmployeeLogic employeeLogincHandelr = new EmployeeLogic();
		EmployeeFullProfileDto employeeFullProfile = employeeLogincHandelr.getEmployeeFullProfile(employeeId);
		if(employeeFullProfile==null){
			return Response.status(204).entity(NO_CONTECT).build();
		}
		Gson g = new Gson();
		String response = g.toJson(employeeFullProfile);
		return Response.status(200).entity(response).build();
	}
	
	
	
	
	
}
