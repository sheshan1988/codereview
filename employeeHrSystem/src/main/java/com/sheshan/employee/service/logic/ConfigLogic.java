package com.sheshan.employee.service.logic;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.sheshan.employee.service.dao.ConfigDao;
import com.sheshan.employee.service.dto.DayDto;
import com.sheshan.employee.service.dto.DayPartialsDto;
import com.sheshan.employee.service.dto.EmployeePositionDto;
import com.sheshan.employee.service.dto.WeekWorkDaysConfigDto;

public class ConfigLogic {

	public ArrayList<EmployeePositionDto> getAllEmployeeStatusConfig() {
		// TODO Auto-generated method stub
		ArrayList<EmployeePositionDto> employeePositionList = new ArrayList<>();
		ResultSet rs = ConfigDao.getAllEmployeePositionList();
		try {
			while (rs.next()) {
				EmployeePositionDto employeePostionDto = new EmployeePositionDto();
				employeePostionDto.setPosition_id(rs.getInt(1));
				employeePostionDto.setPosition_title(rs.getString(2));
				employeePositionList.add(employeePostionDto);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return employeePositionList;		
	}

	public boolean updateWeekWorkingDays(WeekWorkDaysConfigDto weekWorkConfigDto) {
		try{
		ArrayList<DayDto> dayList = weekWorkConfigDto.getWorkdays();
		ArrayList<DayPartialsDto> daypartilsList = weekWorkConfigDto.getWorkhours();
		ConfigDao.updateWeekWorkingDays(dayList);
		ConfigDao.updateWorkingDayConfig(daypartilsList);
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public WeekWorkDaysConfigDto getWeekWorkConfigDetails() {
		WeekWorkDaysConfigDto workDaysConfigDto = new WeekWorkDaysConfigDto();
		ArrayList<DayDto> dayList = ConfigDao.getWeekWorkingDays();
		ArrayList<DayPartialsDto> daypartilsList = ConfigDao.getkWorkingDayConfig();
		workDaysConfigDto.setGrace_period("2");
		workDaysConfigDto.setWorkdays(dayList);
		workDaysConfigDto.setWorkhours(daypartilsList);
		return workDaysConfigDto;
	}
	
	

}
