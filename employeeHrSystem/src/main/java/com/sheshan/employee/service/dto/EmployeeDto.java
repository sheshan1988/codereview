package com.sheshan.employee.service.dto;

import java.util.ArrayList;

public class EmployeeDto {
	private int employeeId;
	private String groupName;
	private String hodName;
	private String email;
	private String profilePicUrl;
	private boolean error; 
	private String errorMessage;
	private ArrayList<LeaveQuotaDto> leaveQuotaList;
	private ArrayList<EmployeeGroupDto> employeeGroupDtoList;
	
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getHodName() {
		return hodName;
	}
	public void setHodName(String hodName) {
		this.hodName = hodName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getProfilePicUrl() {
		return profilePicUrl;
	}
	public void setProfilePicUrl(String profilePicUrl) {
		this.profilePicUrl = profilePicUrl;
	}
	
	public boolean isError() {
		return error;
	}
	public void setError(boolean error) {
		this.error = error;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public ArrayList<LeaveQuotaDto> getLeaveQuotaList() {
		return leaveQuotaList;
	}
	public void setLeaveQuotaList(ArrayList<LeaveQuotaDto> leaveQuotaList) {
		this.leaveQuotaList = leaveQuotaList;
	}
	public ArrayList<EmployeeGroupDto> getGroupList() {
		return employeeGroupDtoList;
	}
	public void setGroupList(ArrayList<EmployeeGroupDto> groupList) {
		this.employeeGroupDtoList = groupList;
	}	
}
