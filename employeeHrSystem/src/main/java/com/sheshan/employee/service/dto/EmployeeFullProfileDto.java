package com.sheshan.employee.service.dto;

public class EmployeeFullProfileDto {

	private int employeeid;
	private String fullname;
	private String email;
	private String dob;
	private String gender;
	private String nic_no;
	private String address;
	private String emergency_contact;
	private String civil_status;
	private String blood_group;
	private String designation;
	private String epfno;
	private String department;
	private String location;
	private String reporting_manager; // When employee inserting Name will contain the corresponding E_id instead of Name
	private String date_of_join;
	private int probation_priod;
	private String date_of_confimation;
	private String date_of_resignation;
	private String user_role;
	private String emp_status;
	private String login_status;
	
	public int getEmployeeid() {
		return employeeid;
	}
	public void setEmployeeid(int employeeid) {
		this.employeeid = employeeid;
	}

	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getNic_no() {
		return nic_no;
	}
	public void setNic_no(String nic_no) {
		this.nic_no = nic_no;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmergency_contact() {
		return emergency_contact;
	}
	public void setEmergency_contact(String emergency_contact) {
		this.emergency_contact = emergency_contact;
	}
	public String getCivil_status() {
		return civil_status;
	}
	public void setCivil_status(String civil_status) {
		this.civil_status = civil_status;
	}
	public String getBlood_group() {
		return blood_group;
	}
	public void setBlood_group(String blood_group) {
		this.blood_group = blood_group;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getEpfno() {
		return epfno;
	}
	public void setEpfno(String epfno) {
		this.epfno = epfno;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getReporting_manager() {
		return reporting_manager;
	}
	public void setReporting_manager(String reporting_manager) {
		this.reporting_manager = reporting_manager;
	}
	public String getDate_of_join() {
		return date_of_join;
	}
	public void setDate_of_join(String date_of_join) {
		this.date_of_join = date_of_join;
	}
	public int getProbation_priod() {
		return probation_priod;
	}
	public void setProbation_priod(int probation_priod) {
		this.probation_priod = probation_priod;
	}
	public String getDate_of_confimation() {
		return date_of_confimation;
	}
	public void setDate_of_confimation(String date_of_confimation) {
		this.date_of_confimation = date_of_confimation;
	}
	public String getDate_of_resignation() {
		return date_of_resignation;
	}
	public void setDate_of_resignation(String date_of_resignation) {
		this.date_of_resignation = date_of_resignation;
	}
	public String getUser_role() {
		return user_role;
	}
	public void setUser_role(String user_role) {
		this.user_role = user_role;
	}
	public String getEmp_status() {
		return emp_status;
	}
	public void setEmp_status(String emp_status) {
		this.emp_status = emp_status;
	}
	public String getLogin_status() {
		return login_status;
	}
	public void setLogin_status(String login_status) {
		this.login_status = login_status;
	}
	
	
}