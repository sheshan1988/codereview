package com.sheshan.employee.service.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.sheshan.employee.service.dto.EmployeeLeaveHistoryDto;
import com.sheshan.employee.service.util.DbConnection;

public class LeaveDao {

	public static boolean enableDisableLeaveTypes(String leaveType, int action) {
		StringBuilder query = new StringBuilder();
		Connection con = null;
		query.append(" UPDATE leave_types");
		query.append(" SET enabled = ? where type = ?");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setInt(1, action);
			preparedStmt.setString(2, leaveType);
			System.out.println(preparedStmt.toString());
			preparedStmt.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static ResultSet getLeaveConfigurationQuotaByLeaveType(String leaveType) {
		ResultSet rs = null;
		Connection con = null;
		StringBuilder query = new StringBuilder();
		query.append(" Select lc.employee_status , lc.value from leave_configuration lc");
		query.append(" inner join leave_types lt on lt.L_id = lc.L_id");
		query.append(" where lt.type = ?");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setString(1, leaveType);
			rs = preparedStmt.executeQuery();
			return rs;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rs;
	}
	
	public static ResultSet getAllLeaveType() {
		ResultSet rs = null;
		Connection con = null;
		StringBuilder query = new StringBuilder();
		query.append(" Select lc.employee_status , lc.value from leave_configuration lc");
		query.append(" inner join leave_types lt on lt.L_id = lc.L_id");
		query.append(" where lt.type = ?");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			rs = preparedStmt.executeQuery();
			return rs;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rs;
	}

	public static ResultSet getLeaveConfigByLeaveType(String leaveType) {
		ResultSet rs = null;
		Connection con = null;
		StringBuilder query = new StringBuilder();
		query.append("Select lt.type , lc.value FROM leave_configuration lc ");
		query.append("inner join leave_types lt on lt.L_id = lc.L_id ");
		query.append("where lc.employee_status = ?");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setString(1, leaveType);
			rs = preparedStmt.executeQuery();
			return rs;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rs;
	}

	public static void updateLeaveConfig(String leave_type, String label, double value) {
		StringBuilder query = new StringBuilder();
		Connection con = null;
		query.append("UPDATE leave_configuration l ");
		query.append("SET l.value = ? ");
		query.append("WHERE l.L_id = (Select L_id from leave_types where type = ? ) ");
		query.append("And l.employee_status = ? ");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setDouble(1, value);
			preparedStmt.setString(2, leave_type);
			preparedStmt.setString(3, label);
			System.out.println(preparedStmt.toString());
			preparedStmt.executeUpdate();
			preparedStmt.closeOnCompletion();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbConnection.closeConnection(con);
		}

	}

	public static ArrayList<EmployeeLeaveHistoryDto> getEmployeeLeaveHistory(int employeeId) {
		StringBuilder query = new StringBuilder();
		ArrayList<EmployeeLeaveHistoryDto> employeeLeaveHistpryDtoList = new ArrayList<>();
		Connection con = null;
		ResultSet rs = null;
		query.append(
				"SELECT  eh.status , eh.start_date , eh.end_date ,eh.total_days , lt.type ,ec.E_id , ec.name  ,eo.name ");
		query.append("FROM hr_system.employee_leave_history eh ");
		query.append("inner join employee e ");
		query.append("on e.E_id = eh.E_id ");
		query.append("inner join employee ec ");
		query.append("on ec.E_id = eh.covering_E_id ");
		query.append("inner join employee eo ");
		query.append("on eo.E_id = eh.Hod_E_id ");
		query.append("inner join leave_types lt ");
		query.append("on lt.L_id = eh.L_id ");
		query.append("Where eh.E_id = ? ");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());

				preparedStmt.setInt(1, employeeId);
				System.out.println(preparedStmt.toString());
				rs = preparedStmt.executeQuery();
				rs.next();
				EmployeeLeaveHistoryDto employeeLeaveHistoryDto = new EmployeeLeaveHistoryDto();
				employeeLeaveHistoryDto.setStatus(rs.getString(1));
				employeeLeaveHistoryDto.setFromdate(rs.getString(2));
				employeeLeaveHistoryDto.setTodate(rs.getString(3));
				employeeLeaveHistoryDto.setLeavecount(rs.getDouble(4));
				employeeLeaveHistoryDto.setLeavetype(rs.getString(5));
				employeeLeaveHistoryDto.setCovering(rs.getString(6) + "-" + rs.getString(7));
				employeeLeaveHistoryDto.setApproved(rs.getString(8));
				employeeLeaveHistpryDtoList.add(employeeLeaveHistoryDto);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbConnection.closeConnection(con);
		}
		return employeeLeaveHistpryDtoList;
	}

}
