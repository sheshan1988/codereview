package com.sheshan.employee.service.dto;

import java.util.ArrayList;

public class WeekWorkDaysConfigDto {
	
	private ArrayList<DayDto> workdays;
	private ArrayList<DayPartialsDto> workhours;
	private String grace_period;
	
	public ArrayList<DayDto> getWorkdays() {
		return workdays;
	}
	public void setWorkdays(ArrayList<DayDto> workdays) {
		this.workdays = workdays;
	}
	
	public ArrayList<DayPartialsDto> getWorkhours() {
		return workhours;
	}
	public void setWorkhours(ArrayList<DayPartialsDto> workhours) {
		this.workhours = workhours;
	}
	public String getGrace_period() {
		return grace_period;
	}
	public void setGrace_period(String grace_period) {
		this.grace_period = grace_period;
	}
	
	

}
