package com.sheshan.employee.service;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.sheshan.employee.service.dto.EmployeeLeaveHistoryDto;
import com.sheshan.employee.service.dto.LeaveConfigByTypeDto;
import com.sheshan.employee.service.dto.LeaveQuotaDto;
import com.sheshan.employee.service.logic.LeaveLogic;

@Path("config/leaves")
public class LeaveService {

	private final String ACTION_ENABLED = "enable";
	private final String ACTION_DISABLED = "disable";

	@GET
	@Path("/toggle")
	@Produces(MediaType.APPLICATION_JSON)
	public Response toggleLeaveByLeaveType(@QueryParam("type") String leaveType, @QueryParam("action") String action) {
		String invalidAction = "{ \"message\" : \"Invalid Action\"}";
		String successMessage = "{ \"success\" : true, \"message\" : \" Leave Type " + leaveType + " " + action
				+ " for the organization\"}";
		String errorMessage = "{ \"success\" : false, \"message\" : \" Updating leave configuration Failed \"}";
		;
		int leaveEnableDisableStatus = 0;
		if (action.equals(ACTION_ENABLED)) {
			leaveEnableDisableStatus = 1;
		} else if (action.equals(ACTION_DISABLED)) {
			leaveEnableDisableStatus = 0;
		} else {
			return Response.status(200).entity(invalidAction).build();
		}
		LeaveLogic leaveLogic = new LeaveLogic();
		boolean status = leaveLogic.enableDisableLeaveType(leaveType, leaveEnableDisableStatus);
		if (status) {
			return Response.status(200).entity(successMessage).build();
		} else {
			return Response.status(200).entity(errorMessage).build();
		}

	}

	@GET
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLeaveCOnfigLeaveType(@QueryParam("type") String leaveType) {
		String errorMessage = "{ \"error\" : true, \"message\" : \" System Fault \"}";
		LeaveLogic leaveLogic = new LeaveLogic();
		LeaveConfigByTypeDto leaveConfigByTypeDto = leaveLogic.getLeaveConfig(leaveType);
		if (leaveConfigByTypeDto == null) {
			return Response.status(200).entity(errorMessage).build();
		}
		Gson g = new Gson();
		String response = g.toJson(leaveConfigByTypeDto);
		return Response.status(200).entity(response).build();

	}

	@POST
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateLeaveConfig(LeaveConfigByTypeDto leaveConfigByTypeDto) {
		LeaveLogic leaveLogic = new LeaveLogic();
		String message;
		boolean status = leaveLogic.updateLeaveConfig(leaveConfigByTypeDto);
		if (status) {
			message = "{\"success\" : true,\n\"message\" : \" " + leaveConfigByTypeDto.getLeave_type()
					+ " leave configuration updated successfully\" }";
			return Response.status(201).entity(message).build();
		} else {
			message = "{\"success\" : false,\n\"message\" : \"Error updating Leave Configuration\" }";
			return Response.status(201).entity(message).build();
		}

	}

	@GET
	@Path("/history")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLeaveHistory(@DefaultValue("0") @QueryParam("emp") String empId) {
		String errorMessage = "{ \"error\" : true, \"message\" : \" System Fault \"}";
		LeaveLogic leaveLogic = new LeaveLogic();
		ArrayList<EmployeeLeaveHistoryDto> employeeLeaveHistpryDtoList = leaveLogic.getLeaveHistory(empId);
		if (employeeLeaveHistpryDtoList == null) {
			return Response.status(200).entity(errorMessage).build();
		}
		Gson g = new Gson();
		String response = g.toJson(employeeLeaveHistpryDtoList);
		return Response.status(200).entity(response).build();

	}
	
	@GET
	@Path("/inventory")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLeaveInventoryForEmployee(@DefaultValue("0") @QueryParam("emp") String empId) {
		String errorMessage = "{ \"error\" : true, \"message\" : \" System Fault \"}";
		LeaveLogic leaveLogic = new LeaveLogic();
		ArrayList<LeaveQuotaDto> leaveQuotaDtoList = leaveLogic.getLeaveInventoryForEmployee(empId);
		if (leaveQuotaDtoList == null) {
			return Response.status(200).entity(errorMessage).build();
		}
		Gson g = new Gson();
		String response = g.toJson(leaveQuotaDtoList);
		return Response.status(200).entity(response).build();

	}

}
