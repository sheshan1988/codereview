package com.sheshan.employee.service;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.sheshan.employee.service.dto.DepartmentDTo;
import com.sheshan.employee.service.dto.SingleDepartmentDto;
import com.sheshan.employee.service.logic.DepartmentLogic;

@Path("/department")
public class DepartmentService {
	
	private final String ADD_EMPLOYE_ACTION = "add";
	private final String REMOVE_EMPLOYEE_ACTION = "remove";

	@GET
	@Path("/get_all")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getemployeesByGroupId(@DefaultValue("") @QueryParam("name") String name,
			@DefaultValue("0") @QueryParam("limit") int limit) {

		DepartmentLogic departmentLogicLogicHandler = new DepartmentLogic();
		ArrayList<DepartmentDTo> employeeDtoList = departmentLogicLogicHandler.getAllHDepartmentsDatawithHodData(name,
				limit);
		Gson g = new Gson();
		String response = g.toJson(employeeDtoList);
		return Response.status(200).entity(response).build();
	}

	@GET
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	public Response employeesByGroupId(@QueryParam("id") int departmentId) {

		DepartmentLogic departmentLogicLogicHandler = new DepartmentLogic();
		SingleDepartmentDto singleDepartmentDto = departmentLogicLogicHandler
				.getAllDataForSingleDepartmentDto(departmentId);
		Gson g = new Gson();
		String response = g.toJson(singleDepartmentDto);
		return Response.status(200).entity(response).build();
	}

	@POST
	@Path("add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addGroup(DepartmentDTo departmentAddDto) {
		DepartmentLogic departmentLogic = new DepartmentLogic();
		String message;
		boolean status = departmentLogic.addEmployeeGroup(departmentAddDto);
		if (status) {
			message = "{\"success\" : true,\n\"message\" : \"New department " + departmentAddDto.getDep_name()
					+ " created successfully\" }";
			return Response.status(201).entity(message).build();
		} else {
			message = "{\"success\" : false,\n\"message\" : \"Error inserting " + departmentAddDto.getDep_name() + "\" }";
			return Response.status(201).entity(message).build();
		}

	}

	@POST
	@Path("update")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateGroup(DepartmentDTo departmentAddDto) {
		DepartmentLogic departmentLogic = new DepartmentLogic();
		String message;
		boolean status = departmentLogic.updateEmployeeGroup(departmentAddDto);
		if (status) {
			message = "{\"success\" : true,\n\"message\" : " + departmentAddDto.getDep_name()
					+ " details updated successfully }";
			return Response.status(201).entity(message).build();
		} else {
			message = "{\"success\" : false,\n\"message\" : \"Error updating " + departmentAddDto.getDep_name() + " }";
			return Response.status(201).entity(message).build();
		}

	}

	@GET
	@Path("/updateEmployee")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addEmployeesToGroup(@QueryParam("emp") int employeeId, @QueryParam("dep") int departmentId,
			@QueryParam("action") String action) {
		String response = null;
		DepartmentLogic departmentLogicLogicHandler = new DepartmentLogic();
		if(action.equals(ADD_EMPLOYE_ACTION)){
			departmentLogicLogicHandler.addEmployeetoDepartmentByid(employeeId , departmentId);
			response = " {\"success\" : true,\n\"message\" : \"Employee " +employeeId+ " added to "+departmentId +" \"}";
		} else if(action.equals(REMOVE_EMPLOYEE_ACTION)){
			departmentLogicLogicHandler.deleteEmployeetoDepartmentByid(employeeId , departmentId);
			response = " {\"success\" : true,\n\"message\" : \"" +employeeId+ " removed from  "+departmentId + "\" }";
		}
		return Response.status(200).entity(response).build();
	}

}
