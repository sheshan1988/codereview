package com.sheshan.employee.service.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sheshan.employee.service.util.DbConnection;

public class EmployeeDao {

	public static ResultSet getVlaidLoginEmployee(String email) {
		ResultSet rs = null;
		Connection con = null;
		String query = "Select * from employee where username=? AND system_status =1";
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query);
			preparedStmt.setString(1, email);
			rs = preparedStmt.executeQuery();
			return rs;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rs;
	}

	public static ResultSet getEmployeeLeaveQuota(int e_id) {
		Connection con = null;
		ResultSet rs = null;
		String query = "Select elq.L_id , lt.type ,elq.value ,elq.remaining_value from employee_leave_quota elq"
				+ " inner join leave_types lt " + "on elq.L_id = lt.L_id" + " where elq.E_id = ?";
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query);
			preparedStmt.setInt(1, e_id);
			rs = preparedStmt.executeQuery();
			return rs;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rs;
	}

	public static ResultSet getEmployeeListByGroupId(String groupId) {
		ResultSet rs = null;
		Connection con = null;
		StringBuilder query = new StringBuilder();
		query.append(
				" SELECT e.E_id as emp_id, username, `status` as emp_status, start_date, system_status, role,  g.name as group_name , g.G_id as group_id ");
		query.append(" FROM employee e");
		query.append(" inner join employee_group eg on e.E_id=eg.E_id");
		query.append(" inner join `group_data` g on g.G_id=eg.G_id");
		query.append(" where g.name = ?");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setString(1, groupId);
			rs = preparedStmt.executeQuery();
			return rs;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rs;
	}

	public static ResultSet getAllEmployee(String name, int limit) {
		ResultSet rs = null;
		Connection con  = null;
		StringBuilder query = new StringBuilder();
		query.append("Select e.E_id,e.name,e.username, g.name from employee e ");
		query.append("inner join employee_group eg ");
		query.append("on eg.E_id = e.E_id ");
		query.append("inner join group_data g ");
		query.append("on g.G_id = eg.G_id ");
		query.append("where e.system_status =1 ");

		if (name != null && limit != 0) {
			query.append("And e.name like '%" + name + "%' limit 0," + limit);
		} else if (name == null && limit != 0) {
			query.append("limit 0," + limit);
		} else if (name != null && limit == 0) {
			query.append("And e.name like '%" + name + "%' limit 0, 50");
		} else {
			query.append("limit 0, 50 ");
		}
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			System.out.println(preparedStmt.toString());
			rs = preparedStmt.executeQuery();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rs;

	}

	public static ResultSet getEmployeefullProfile(int employeeID) {
		ResultSet rs = null;
		Connection con = null;
		StringBuilder query = new StringBuilder();
		query.append("Select e.E_id , e.full_name, e.username,epd.dob ,epd.gender ,epd.nic_no , epd.address ,");
		query.append("epd.emergency_contact , epd.civil_status ,epd.blood_group ,epd.designation ,epd.epfno ,");
		query.append(
				"epd.location , epd.date_of_join , epd.probation_priod ,epd.date_of_confimation ,epd.date_of_resignation ,g.name ,eg.role ,epd.reporting_manager ");
		query.append("from employee e ");
		query.append("inner join employee_personal_data epd on epd.E_id = e.E_id ");
		query.append("inner join employee_group eg on eg.E_id = e.E_id ");
		query.append("inner join group_data g on g.G_id = eg.G_id ");
		query.append("where e.system_status =1 ");
		query.append("And e.E_id =?");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setInt(1, employeeID);
			System.out.println(preparedStmt.toString());
			rs = preparedStmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		return rs;
	}

	public static ResultSet insertDatatoEmployeeTable(String username, String status, Date startDate, int systemstatus,
			String name, String fullname) {
		ResultSet rs = null;
		Connection con = null;
		StringBuilder query = new StringBuilder();
		query.append("INSERT INTO employee ");
		query.append("( username , status , start_date , system_status ,name ,full_name ) ");
		query.append("VALUES (? ,? ,? ,? ,? ,?)");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setString(1, username);
			preparedStmt.setString(2, status);
			preparedStmt.setDate(3, startDate);
			preparedStmt.setInt(4, systemstatus);
			preparedStmt.setString(5, name);
			preparedStmt.setString(6, fullname);
			System.out.println(preparedStmt.toString());
			preparedStmt.executeUpdate();
			rs = preparedStmt.executeQuery("SELECT LAST_INSERT_ID()");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rs;
	}

	public static boolean insertDatatoEMployeeGroupTable(int employeeId, String groupName, String employeeRole) {
		StringBuilder query = new StringBuilder();
		Connection con = null;
		query.append("INSERT INTO employee_group ");
		query.append("( E_id , G_id , role ) ");
		query.append("VALUES (? ,(Select G_id from group_data where name= ? ) ,? )");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setInt(1, employeeId);
			preparedStmt.setString(2, groupName);
			preparedStmt.setString(3, employeeRole);
			System.out.println(preparedStmt.toString());
			preparedStmt.executeUpdate();
			preparedStmt.closeOnCompletion();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			DbConnection.closeConnection(con);
		}
		return true;
	}

	public static boolean insertDatatoEmployeePerosnalTable(Date dob, String gender, String nic_no, String address,
			String emergency_contact, String civil_status, String blood_group, String designation, String epfno,
			Date date_of_join, Date date_of_confimation, Date date_of_resignation, int E_id, int probation_priod,
			String location, int reportingMangerID) {
		Connection con = null;
		StringBuilder query = new StringBuilder();
		query.append("INSERT INTO employee_personal_data ");
		query.append(
				"( dob, gender, nic_no, address, emergency_contact, civil_status, blood_group, designation, epfno, date_of_join, date_of_confimation, date_of_resignation, E_id, probation_priod, location, reporting_manager ) ");
		query.append(
				"VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, (Select name from employee where E_id = ? )) ");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setDate(1, dob);
			preparedStmt.setString(2, gender);
			preparedStmt.setString(3, nic_no);
			preparedStmt.setString(4, address);
			preparedStmt.setString(5, emergency_contact);
			preparedStmt.setString(6, civil_status);
			preparedStmt.setString(7, blood_group);
			preparedStmt.setString(8, designation);
			preparedStmt.setString(9, epfno);
			preparedStmt.setDate(10, date_of_join);
			preparedStmt.setDate(11, date_of_confimation);
			preparedStmt.setDate(12, date_of_resignation);
			preparedStmt.setInt(13, E_id);
			preparedStmt.setInt(14, probation_priod);
			preparedStmt.setString(15, location);
			preparedStmt.setInt(16, reportingMangerID);
			System.out.println(preparedStmt.toString());
			preparedStmt.executeUpdate();
			preparedStmt.closeOnCompletion();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			DbConnection.closeConnection(con);
		}
		return true;
	}

	public static boolean updateEmployeeTable(String username, String status, Date startDate, int systemstatus,
			String name, String fullname, int Eid) {
		StringBuilder query = new StringBuilder();
		Connection con = null;
		query.append("UPDATE employee ");
		query.append("SET username = ?,status = ? ,start_date = ? ,system_status = ? ,name =? ,full_name = ? ");
		query.append("WHERE E_id = ? ");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setString(1, username);
			preparedStmt.setString(2, status);
			preparedStmt.setDate(3, startDate);
			preparedStmt.setInt(4, systemstatus);
			preparedStmt.setString(5, name);
			preparedStmt.setString(6, fullname);
			preparedStmt.setInt(7, Eid);
			System.out.println(preparedStmt.toString());
			preparedStmt.executeUpdate();
			preparedStmt.closeOnCompletion();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} finally {
			DbConnection.closeConnection(con);
		}
		return true;
	}

	public static boolean updateDataEmployeeGroupTable(int employeeId, String groupName, String employeeRole) {
		Connection con = null;
		StringBuilder query = new StringBuilder();
		query.append("UPDATE employee_group ");
		query.append("SET  G_id = ( Select G_id from group_data where name= ? ), role = ? ");
		query.append("WHERE E_id = ?;");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setString(1, groupName);
			preparedStmt.setString(2, employeeRole);
			preparedStmt.setInt(3, employeeId);
			System.out.println(preparedStmt.toString());
			preparedStmt.executeUpdate();
			preparedStmt.closeOnCompletion();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			DbConnection.closeConnection(con);
		}
		return true;
	}

	public static boolean updateDatatoEmployeePerosnalTable(Date dob, String gender, String nic_no, String address,
			String emergency_contact, String civil_status, String blood_group, String designation, String epfno,
			Date date_of_join, Date date_of_confimation, Date date_of_resignation, int E_id, int probation_priod,
			String location, int reportingMangerID) {
		StringBuilder query = new StringBuilder();
		Connection con = null;
		query.append("UPDATE employee_personal_data ");
		query.append("SET dob= ?, gender=?, nic_no  = ?, address  = ?, emergency_contact  = ?, civil_status  = ?, ");
		query.append("blood_group  = ?, designation  = ?, epfno  = ?, date_of_join  = ?, date_of_confimation  = ?, ");
		query.append(
				"date_of_resignation  = ?, probation_priod  = ?, location = ?, reporting_manager = ( Select name from employee where E_id = ? ) ");
		query.append("WHERE E_id = ?");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setDate(1, dob);
			preparedStmt.setString(2, gender);
			preparedStmt.setString(3, nic_no);
			preparedStmt.setString(4, address);
			preparedStmt.setString(5, emergency_contact);
			preparedStmt.setString(6, civil_status);
			preparedStmt.setString(7, blood_group);
			preparedStmt.setString(8, designation);
			preparedStmt.setString(9, epfno);
			preparedStmt.setDate(10, date_of_join);
			preparedStmt.setDate(11, date_of_confimation);
			preparedStmt.setDate(12, date_of_resignation);
			preparedStmt.setInt(13, probation_priod);
			preparedStmt.setString(14, location);
			preparedStmt.setInt(15, reportingMangerID);
			preparedStmt.setInt(16, E_id);
			System.out.println(preparedStmt.toString());
			preparedStmt.executeUpdate();
			preparedStmt.closeOnCompletion();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			DbConnection.closeConnection(con);
		}
		return true;
	}

	public static void DeleteFromEmployeeTable(int employeeId) {
		StringBuilder query = new StringBuilder();
		Connection con = null;
		query.append("Delete from employee where E_id = ?");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setInt(1, employeeId);
			System.out.println(preparedStmt.toString());
			preparedStmt.executeUpdate();
			preparedStmt.closeOnCompletion();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbConnection.closeConnection(con);
		}
	}

	public static void DeleteFromEmployeeGroupTable(int employeeId) {
		StringBuilder query = new StringBuilder();
		Connection con = null;
		query.append("Delete from employee_group where E_id = ?");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setInt(1, employeeId);
			System.out.println(preparedStmt.toString());
			preparedStmt.executeUpdate();
			preparedStmt.closeOnCompletion();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbConnection.closeConnection(con);
		}
	}

}
