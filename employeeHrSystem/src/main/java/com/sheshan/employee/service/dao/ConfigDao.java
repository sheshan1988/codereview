package com.sheshan.employee.service.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import com.sheshan.employee.service.dto.DayDto;
import com.sheshan.employee.service.dto.DayPartialsDto;
import com.sheshan.employee.service.util.DbConnection;

public class ConfigDao {

	public static ResultSet getAllEmployeePositionList() {
		ResultSet rs = null;
		StringBuilder query = new StringBuilder();
		query.append(" SELECT * FROM employee_position;");
		try {
			Connection con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			rs = preparedStmt.executeQuery();
			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}

	public static void updateWeekWorkingDays(ArrayList<DayDto> dayList) {
		StringBuilder query = new StringBuilder();
		Connection con = null;
		query.append("UPDATE working_days");
		query.append(
				" SET day = ? , status =? ,  W_d_t_id = ( Select W_d_t_id FROM work_day_time_config WHERE  name = ? )");
		query.append(" WHERE day=?");
		Iterator<DayDto> iterator = dayList.iterator();
		while (iterator.hasNext()) {
			DayDto workDay = iterator.next();
			try {
				con = DbConnection.getDbconnection();
				PreparedStatement preparedStmt = con.prepareStatement(query.toString());
				preparedStmt.setString(1, workDay.getDay());
				preparedStmt.setString(2, String.valueOf(workDay.isWorking()));
				preparedStmt.setString(3, workDay.getWork());
				preparedStmt.setString(4, workDay.getDay());
				System.out.println(preparedStmt.toString());
				preparedStmt.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				DbConnection.closeConnection(con);
			}
		}
	}

	public static void updateWorkingDayConfig(ArrayList<DayPartialsDto> daypartilsList) {
		StringBuilder query = new StringBuilder();
		Connection con = null;
		query.append("UPDATE work_day_time_config");
		query.append(" SET name = ? , start_time =?  ,end_time=? ");
		query.append(" WHERE name= ?");
		Iterator<DayPartialsDto> iterator = daypartilsList.iterator();
		while (iterator.hasNext()) {
			DayPartialsDto dayPartilas = iterator.next();
			try {
				con = DbConnection.getDbconnection();
				PreparedStatement preparedStmt = con.prepareStatement(query.toString());
				preparedStmt.setString(1, dayPartilas.getWork());
				preparedStmt.setString(2, dayPartilas.getStart());
				preparedStmt.setString(3, dayPartilas.getEnd());
				preparedStmt.setString(4, dayPartilas.getWork());
				System.out.println(preparedStmt.toString());
				preparedStmt.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				DbConnection.closeConnection(con);
			}
		}
	}

	public static ArrayList<DayDto> getWeekWorkingDays() {
		ArrayList<DayDto> workingDayList = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		Connection con = null;	
		query.append("SELECT wd.day , wd.status , wc.name FROM working_days wd ");
		query.append("inner join work_day_time_config wc ");
		query.append("on wd.W_d_t_id = wc.W_d_t_id");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			System.out.println(preparedStmt.toString());
			ResultSet rs = preparedStmt.executeQuery();
			while (rs.next()) {
				DayDto dayDto = new DayDto();
				dayDto.setDay(rs.getString(1));
				dayDto.setWorking(Boolean.parseBoolean(rs.getString(2)));
				dayDto.setWork(rs.getString(3));
				workingDayList.add(dayDto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbConnection.closeConnection(con);
		}
		return workingDayList;
	}
	
	public static ArrayList<DayPartialsDto> getkWorkingDayConfig() {
		ArrayList<DayPartialsDto> workingDayConfigList = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		Connection con = null;	
		query.append("Select name , start_time , end_time from  work_day_time_config");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			System.out.println(preparedStmt.toString());
			ResultSet rs = preparedStmt.executeQuery();
			while (rs.next()) {
				DayPartialsDto dayPartialDto = new DayPartialsDto();
				dayPartialDto.setWork(rs.getString(1));
				dayPartialDto.setStart(rs.getString(2));
				dayPartialDto.setEnd(rs.getString(3));
				workingDayConfigList.add(dayPartialDto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbConnection.closeConnection(con);
		}
		return workingDayConfigList;
	}

}
