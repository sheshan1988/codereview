package com.sheshan.employee.service.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sheshan.employee.service.util.DbConnection;

public class EmployeeGroupDao {
	
	public static ResultSet getEmployeeGroupHODandGroupName(int employeeId) {
		ResultSet rs = null;
		Connection con = null; 
		StringBuilder query = new StringBuilder();
		query.append(" Select g.name,e.name  from employee_group eg");
		query.append(" inner join group_data g on g.G_id = eg.G_id");
		query.append(" inner join employee e on e.E_id = eg.E_id");
		query.append(" where e.system_status =1 ");
		query.append(" And eg.role = 'hod'");
		query.append(" And eg.G_id = (Select G_id from employee_group where E_id = ?)");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString() );
			preparedStmt.setInt(1, employeeId);
			rs = preparedStmt.executeQuery();
			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}
	
	public static ResultSet getAllGroupData() {
		ResultSet rs = null;
		Connection con = null;
		StringBuilder query = new StringBuilder();
		query.append(" Select * from group_data;");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString() );
			rs = preparedStmt.executeQuery();
			preparedStmt.closeOnCompletion();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}
	
	public static ResultSet getGroupDataByGroupName(String name) {
		ResultSet rs = null;
		Connection con = null; 
		StringBuilder query = new StringBuilder();
		query.append(" Select * from group_data where name = ?");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString() );
			preparedStmt.setString(1, name);
			rs = preparedStmt.executeQuery();
			System.out.println(preparedStmt.toString());
			preparedStmt.closeOnCompletion();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}
	
	
	public static ResultSet getHODDataForGroup(int groupId ,String name ,int limit ) {
		ResultSet rs = null;
		Connection con = null;
		System.out.println(groupId +"~~~~~~"+name + "~~~~~~"+ limit);
		StringBuilder query = new StringBuilder();		
		
		if (!name.equals("") && limit != 0) {
			query.append(" SELECT ( SELECT g.E_id FROM   employee_group g inner join employee e on e.E_id = g.E_id inner join group_data gd on gd.G_id = g.G_id where g.role='hod' And e.system_status =1 And gd.name ='"+name+"') AS hod,");
			query.append(" ( SELECT g.E_id FROM   employee_group g inner join employee e on e.E_id = g.E_id inner join group_data gd on gd.G_id = g.G_id where g.role='acting_hod' And e.system_status =1 And gd.name ='"+name+"') AS acting_hod  limit 0,"+limit) ;
		} else if (name.equals("") && limit != 0) {
			query.append(" SELECT ( SELECT g.E_id FROM   employee_group g inner join employee e on e.E_id = g.E_id where  g.role='hod' And e.system_status =1) AS hod, ");
			query.append(" ( SELECT g.E_id FROM   employee_group g inner join employee e on e.E_id = g.E_id where  g.role='acting_hod' And e.system_status =1) AS acting_hod limit 0,"+limit) ;
		} else if (!name.equals("") && limit == 0){
			query.append(" SELECT ( SELECT g.E_id FROM   employee_group g inner join employee e on e.E_id = g.E_id inner join group_data gd on gd.G_id = g.G_id where  g.role='hod' And e.system_status =1 And gd.name ='"+name+"') AS hod,");
			query.append(" ( SELECT g.E_id FROM   employee_group g inner join employee e on e.E_id = g.E_id inner join group_data gd on gd.G_id = g.G_id where g.role='acting_hod' And e.system_status =1 And gd.name ='"+name+"') AS acting_hod  limit 0,50") ;
		} else {
			query.append(" SELECT ( SELECT g.E_id FROM   employee_group g inner join employee e on e.E_id = g.E_id inner join group_data gd on gd.G_id = g.G_id where g.G_id ="+groupId+" And g.role='hod' And e.system_status =1 ) AS hod,");
			query.append(" ( SELECT g.E_id FROM   employee_group g inner join employee e on e.E_id = g.E_id inner join group_data gd on gd.G_id = g.G_id where g.G_id ="+groupId+" And g.role='acting_hod' And e.system_status =1 ) AS acting_hod  limit 0,50") ;
		}
		
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			System.out.println(preparedStmt.toString());
			rs = preparedStmt.executeQuery();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}
	
	
	public static ResultSet getGroupDataByGroupId(int id) {
		ResultSet rs = null;
		Connection con = null; 
		StringBuilder query = new StringBuilder();
		query.append(" Select * from group_data where G_id = ? ");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setInt(1, id);
			rs = preparedStmt.executeQuery();
			System.out.println(preparedStmt.toString());
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		return rs;
	}
	
	public static ResultSet getEMployeeDataByGroupId(int id , String employeeType) {
		ResultSet rs = null;
		Connection con = null; 
		StringBuilder query = new StringBuilder();
		query.append(" Select e.E_id , e.username ,e.name from group_data gd");
		query.append(" inner join employee_group eg");
		query.append(" on eg.G_id = gd.G_id");
		query.append(" inner join employee e");
		query.append(" on e.E_id = eg.E_id");
		query.append(" where gd.G_id = ?  And eg.role= ? ");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setInt(1, id);
			preparedStmt.setString(2, employeeType);
			rs = preparedStmt.executeQuery();
			System.out.println(preparedStmt.toString());
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	} 
	
	public static ResultSet getEMployeeDataByEMployeePermission(int id , String employeePermission) {
		ResultSet rs = null;
		Connection con = null;
		StringBuilder query = new StringBuilder();
		query.append(" Select e.E_id , e.username ,e.name from group_data gd");
		query.append(" inner join employee_group eg");
		query.append(" on eg.G_id = gd.G_id");
		query.append(" inner join employee e");
		query.append(" on e.E_id = eg.E_id");
		query.append(" inner join employee_permission ep");
		query.append(" on e.E_id = ep.E_id");
		query.append(" where gd.G_id = ?  And ep.role= ? And ep.G_id = ? ");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setInt(1, id);
			preparedStmt.setString(2, employeePermission);
			preparedStmt.setInt(3, id);
			rs = preparedStmt.executeQuery();
			System.out.println(preparedStmt.toString());
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}

	
	
	public static ResultSet insertDataToGroupDataTable(String name){
		Connection con = null;
		ResultSet rs = null;
		StringBuilder query = new StringBuilder();
		query.append("INSERT INTO group_data (name) ");
		query.append("VALUES ( ? ) ");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setString(1, name);
			System.out.println(preparedStmt.toString());
			preparedStmt.executeUpdate();
			rs = preparedStmt.executeQuery("SELECT LAST_INSERT_ID()");
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		return rs;
	}
	
	public static boolean inserDatatoEmployeeGroupTable(int employeeId , int groupId , String employeeRole){
		Connection con = null;
		StringBuilder query = new StringBuilder();
		query.append("INSERT INTO employee_group ( E_id , G_id , role)");
		query.append("VALUES ( ? , ? , ? ) ");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setInt(1, employeeId);
			preparedStmt.setInt(2, groupId);
			preparedStmt.setString(3, employeeRole);
			System.out.println(preparedStmt.toString());
			preparedStmt.executeUpdate();
			preparedStmt.closeOnCompletion();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			DbConnection.closeConnection(con);
		}
		return true;
	}
	
	public static boolean inserDatatoEmployeePermissionTable(int employeeId ,String permission ,int groupId){
		Connection con = null;
		StringBuilder query = new StringBuilder();
		query.append("INSERT INTO employee_permission ( E_id ,role, G_id )");
		query.append("VALUES ( ? , ? , ? ) ");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setInt(1, employeeId);
			preparedStmt.setString(2, permission);
			preparedStmt.setInt(3, groupId);
			System.out.println(preparedStmt.toString());
			preparedStmt.executeUpdate();
			preparedStmt.closeOnCompletion();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			DbConnection.closeConnection(con);
		}
		return true;
	}
	
	public static boolean updateDataGroupDataTable(String name , int groupId){
		Connection con = null;
		StringBuilder query = new StringBuilder();
		query.append("UPDATE group_data ");
		query.append("SET name = ? ");
		query.append("WHERE G_id = ?");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setString(1, name);
			preparedStmt.setInt(2, groupId);
			System.out.println(preparedStmt.toString());
			preparedStmt.executeUpdate();
			preparedStmt.closeOnCompletion();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			DbConnection.closeConnection(con);
		}
		return false;
	}
	
	public static boolean updateDataEmployeeGroupTable(int employeeId , int groupId , String employeeRole){
		Connection con = null;
		StringBuilder query = new StringBuilder();
		query.append("UPDATE employee_group ");
		query.append("SET E_id = ? , role = ? ");
		query.append("WHERE G_id = ? AND role = ?");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setInt(1, employeeId);	
			preparedStmt.setString(2, employeeRole);
			preparedStmt.setInt(3, groupId);
			preparedStmt.setString(4, employeeRole);
			System.out.println(preparedStmt.toString());
			preparedStmt.executeUpdate();
			preparedStmt.closeOnCompletion();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			DbConnection.closeConnection(con);
		}
		return true;
	}
	
	public static boolean updateDataEmployeePermissionTable(int employeeIDbefore , int employeeId ,String permission ,int groupId){
		Connection con = null;
		StringBuilder query = new StringBuilder();
		query.append("UPDATE employee_permission ");
		query.append("SET E_id = ? , role = ?");
		query.append("WHERE G_id = ? And E_id = ? ");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setInt(1, employeeId);
			preparedStmt.setString(2, permission);
			preparedStmt.setInt(3, groupId);
			preparedStmt.setInt(4, employeeIDbefore);
			System.out.println(preparedStmt.toString());
			preparedStmt.executeUpdate();
			preparedStmt.closeOnCompletion();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			DbConnection.closeConnection(con);
		}
		return true;
	}
	
	public static void DeleteFromEmployeeGroupTable(int groupID){
		StringBuilder query = new StringBuilder();
		Connection con = null;
		query.append("Delete from employee_group where G_id = ?");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setInt(1, groupID);
			System.out.println(preparedStmt.toString());
			preparedStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbConnection.closeConnection(con);
		}
	}
	
	public static void DeleteFromGroupDataTablee(int groupID){
		StringBuilder query = new StringBuilder();
		Connection con = null;
		query.append("Delete from group_data where G_id = ?");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setInt(1, groupID);
			System.out.println(preparedStmt.toString());
			preparedStmt.executeUpdate();
			preparedStmt.closeOnCompletion();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbConnection.closeConnection(con);
		}
	}
	 
	public static void insertEmployeeToEmployeeGroupTable(int employeeId , int grpupId){
		Connection con = null;
		StringBuilder query = new StringBuilder();
		query.append("INSERT INTO employee_group (E_id, G_id, role) ");
		query.append("SELECT * FROM (SELECT ?, ?, 'employee') AS tmp ");
		query.append("WHERE NOT EXISTS ( SELECT E_id FROM employee_group WHERE E_id = ? ) LIMIT 1");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setInt(1, employeeId);
			preparedStmt.setInt(2, grpupId);
			preparedStmt.setInt(3, employeeId);
			System.out.println(preparedStmt.toString());
			preparedStmt.executeUpdate();
			preparedStmt.closeOnCompletion();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbConnection.closeConnection(con);
		}
	}
	
	public static void deleteEmployeeFromEmployeeGroupTable(int employeeId , int grpupId){
		Connection con = null;
		StringBuilder query = new StringBuilder();
		query.append("Delete from employee_group where E_id = ? AND G_id = ?;");
		try {
			con = DbConnection.getDbconnection();
			PreparedStatement preparedStmt = con.prepareStatement(query.toString());
			preparedStmt.setInt(1, employeeId);
			preparedStmt.setInt(2, grpupId);
			System.out.println(preparedStmt.toString());
			preparedStmt.executeUpdate();
			preparedStmt.closeOnCompletion();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbConnection.closeConnection(con);
		}
	}

}







