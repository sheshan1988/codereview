package com.sheshan.employee.service;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.sheshan.employee.service.dto.EmployeePositionDto;
import com.sheshan.employee.service.dto.WeekWorkDaysConfigDto;
import com.sheshan.employee.service.logic.ConfigLogic;



@Path("config/system")
public class ConfigService {
	
	@GET
	@Path("/empPositions")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLeaveCOnfigLeaveType() {
		ConfigLogic configLogic = new ConfigLogic();
		ArrayList<EmployeePositionDto> employeePositionList = configLogic.getAllEmployeeStatusConfig();
		Gson g = new Gson();
		String response = g.toJson(employeePositionList);
		return Response.status(200).entity(response).build();
	}
	
	
	@GET
	@Path("/getWorkConfig")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getWorkingDayCongig() {
		
		ConfigLogic configLogic = new ConfigLogic();
		WeekWorkDaysConfigDto weekWorkDayConfig = configLogic.getWeekWorkConfigDetails();
		Gson g = new Gson();
		String response = g.toJson(weekWorkDayConfig);
		return Response.status(200).entity(response).build();
	}
	
	@POST
	@Path("/updateWorkConfig")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateWorkingDayConfig(WeekWorkDaysConfigDto weekWorkConfigDto) {
		String message = "{"+
			   "\"success\" : true,"+
			   "\"message\" : \"Work details configured successfully\""+
			"}";
		
		ConfigLogic configLogic = new ConfigLogic();
		boolean result = configLogic.updateWeekWorkingDays(weekWorkConfigDto);
		if(result){
			return Response.status(200).entity(message).build();
			
		}else{
			return Response.status(201).entity("status:Error").build();
		}

	}
	
	
	
	

	

}
