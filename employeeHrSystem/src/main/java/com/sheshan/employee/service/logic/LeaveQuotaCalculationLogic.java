package com.sheshan.employee.service.logic;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import com.sheshan.employee.service.dao.LeaveDao;

public class LeaveQuotaCalculationLogic {

	public double generatePermenetLeaveQuota(Date startDate) {
		int currrentQuarter = getRemainingQuartersOfTheYear(startDate);
		double days =getAvailableLeavesByTypeForEmployee("annual");
		return days*currrentQuarter;
	}

	@SuppressWarnings("static-access")
	private int getRemainingQuartersOfTheYear(Date startDate) {
		/*
		 * Q1 = 4 , Q2 = 3 ,Q3 =2 , Q4 =1
		 */
		Date sysdate = new Date();
		Calendar sysCalendar = Calendar.getInstance();
		sysCalendar.setTime(sysdate);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);

		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		
		// This returns how many quarters remain in Year so employee will get Annual leave X returning value
		if (year == sysCalendar.get(sysCalendar.YEAR)) {
			if (month >= Calendar.JANUARY && month <= Calendar.MARCH) {
				return 4;
			} else if (month >= Calendar.APRIL && month <= Calendar.JUNE) {
				return 3;
			} else if (month >= Calendar.JULY && month <= Calendar.SEPTEMBER) {
				return 2;
			} else {
				return 1;

			}
		}
		return 0;
	}

	private double getAvailableLeavesByTypeForEmployee(String leaveType) {
		ResultSet rs = LeaveDao.getLeaveConfigurationQuotaByLeaveType(leaveType);
		try {
			rs.next();
			if (rs.getDouble(2)!= 0) {
					return rs.getDouble(2);
			} 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

}
