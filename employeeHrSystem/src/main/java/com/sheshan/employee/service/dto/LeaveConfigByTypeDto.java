package com.sheshan.employee.service.dto;

import java.util.ArrayList;

public class LeaveConfigByTypeDto {
	
	private String leave_type;
	
	private ArrayList<LeaveConfigDto> config;

	public String getLeave_type() {
		return leave_type;
	}

	public void setLeave_type(String leave_type) {
		this.leave_type = leave_type;
	}

	public ArrayList<LeaveConfigDto> getConfig() {
		return config;
	}

	public void setConfig(ArrayList<LeaveConfigDto> config) {
		this.config = config;
	}
	
	

}
