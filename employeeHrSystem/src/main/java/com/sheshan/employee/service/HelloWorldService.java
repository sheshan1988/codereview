package com.sheshan.employee.service;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.sheshan.employee.service.logic.GoogleSheetAcessor;
import com.sheshan.employee.service.util.DbConnection;


@Path("/system")
public class HelloWorldService {

	@GET
	@Path("/test/db")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMsg() {
		String output=null;
		Connection con  = DbConnection.getDbconnection();
		if(con!=null){
			output= "Db Conection : Ok";
		}else{
			output= "Db Conection : FAILED";
		}
		return Response.status(200).entity(output).build();	

	}
	
	
	@GET
	@Path("/test/api")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTestAPi() {
		 Sheets service;
			try {
				service = GoogleSheetAcessor.getSheetsService();
		
	        // Prints the names and majors of students in a sample spreadsheet:
	        // https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
			// https://docs.google.com/spreadsheets/d/1bI59tIQ6x4b4TUN6ugJ0lU0dwrjzGYxQnxuZq-TS2K4/edit#gid=0
	        String spreadsheetId = "1bI59tIQ6x4b4TUN6ugJ0lU0dwrjzGYxQnxuZq";
	        String range = "Class Data!A2:E";
	        ValueRange response = service.spreadsheets().values()
	            .get(spreadsheetId, range)
	            .execute();
	        List<List<Object>> values = response.getValues();
	        if (values == null || values.size() == 0) {
	            System.out.println("No data found.");
	        } else {
	          System.out.println("Name, Major");
	          for (@SuppressWarnings("rawtypes") List row : values) {
	            // Print columns A and E, which correspond to indices 0 and 4.
	            System.out.printf("%s, %s\n", row.get(0), row.get(4));
	          }
	        }
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		return Response.status(200).entity("").build();	

	}
	
	
}
