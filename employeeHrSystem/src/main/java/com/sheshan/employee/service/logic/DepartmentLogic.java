package com.sheshan.employee.service.logic;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.sheshan.employee.service.dao.EmployeeGroupDao;
import com.sheshan.employee.service.dto.DepartmentDTo;
import com.sheshan.employee.service.dto.GetAllEmployeeDto;
import com.sheshan.employee.service.dto.SingleDepartmentDto;

public class DepartmentLogic {

	public ArrayList<DepartmentDTo> getAllHDepartmentsDatawithHodData(String name, int limit) {
		int groupId;
		ArrayList<DepartmentDTo> departmentDtoList = new ArrayList<>();
		ResultSet rs = EmployeeGroupDao.getAllGroupData();
		try {
			if (name.equals("") && limit == 0) {
				while (rs.next()) {
					groupId = rs.getInt(1);
					ResultSet hodDataSet = EmployeeGroupDao.getHODDataForGroup(groupId, name, limit);
					hodDataSet.next();
					DepartmentDTo newDepartmentDto = new DepartmentDTo();
					newDepartmentDto.setDep_id(groupId);
					newDepartmentDto.setDep_name(rs.getString(2));
					newDepartmentDto.setDep_hod(hodDataSet.getInt(1));
					newDepartmentDto.setActing_hod(hodDataSet.getInt(2));
					departmentDtoList.add(newDepartmentDto);
				}
				rs.close();
			} else {
				ResultSet results = EmployeeGroupDao.getGroupDataByGroupName(name);
				results.next();
				groupId = results.getInt(1);
				ResultSet hodDataSet = EmployeeGroupDao.getHODDataForGroup(groupId, name, limit);
				hodDataSet.next();
				DepartmentDTo newDepartmentDto = new DepartmentDTo();
				newDepartmentDto.setDep_id(groupId);
				newDepartmentDto.setDep_name(results.getString(2));
				newDepartmentDto.setDep_hod(hodDataSet.getInt(1));
				newDepartmentDto.setActing_hod(hodDataSet.getInt(2));
				departmentDtoList.add(newDepartmentDto);
				results.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return departmentDtoList;
		}
		return departmentDtoList;
	}

	public SingleDepartmentDto getAllDataForSingleDepartmentDto(int groupId) {
		SingleDepartmentDto singleDepartmentDto = new SingleDepartmentDto();
		ResultSet rs = EmployeeGroupDao.getGroupDataByGroupId(groupId);
		try {
			rs.next();
			singleDepartmentDto.setDep_id(rs.getInt(1));
			singleDepartmentDto.setDep_name(rs.getString(2));
			singleDepartmentDto.setStatus("active");
			singleDepartmentDto.setDep_hod(getEmployeeDataForDepartmentByEmployeeType(groupId, "hod"));
			singleDepartmentDto.setActing_hod(getEmployeeDataForDepartmentByEmployeeType(groupId, "acting_hod"));
			singleDepartmentDto.setEmployees(getEmployeeListForDepartmentByEmployeeType(groupId, "employee"));
			singleDepartmentDto
					.setReport_viewer(getEmployeeListForDepartmentByEmployeeSystemPermission(groupId, "report_viwer"));
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return singleDepartmentDto;
	}

	public boolean addEmployeeGroup(DepartmentDTo addDepartmentDto) {
		ResultSet rs = EmployeeGroupDao.insertDataToGroupDataTable(addDepartmentDto.getDep_name());
		int groupId;
		try {
			rs.next();
			groupId = rs.getInt(1);
			rs.close();
			boolean hodInsertStatus = EmployeeGroupDao.inserDatatoEmployeeGroupTable(addDepartmentDto.getDep_hod(),
					groupId, "hod");
			boolean actingHodInsertStatus = EmployeeGroupDao
					.inserDatatoEmployeeGroupTable(addDepartmentDto.getDep_hod(), groupId, "acting_hod");
			if (!hodInsertStatus || !actingHodInsertStatus) {
				EmployeeGroupDao.DeleteFromEmployeeGroupTable(groupId);
				EmployeeGroupDao.DeleteFromGroupDataTablee(groupId);
				return false;
			}
			int[] reprtViwers = addDepartmentDto.getReport_viewer();
			for (int i = 0; i < reprtViwers.length; i++) {
				EmployeeGroupDao.inserDatatoEmployeePermissionTable(reprtViwers[i], "report_viwer", groupId);
			}
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean updateEmployeeGroup(DepartmentDTo departmentUpdateDto) {
		boolean updateGroupHodStatus = EmployeeGroupDao.updateDataEmployeeGroupTable(departmentUpdateDto.getDep_hod(),
				departmentUpdateDto.getDep_id(), "hod");
		boolean updateGroupActingHodStatus = EmployeeGroupDao.updateDataEmployeeGroupTable(
				departmentUpdateDto.getActing_hod(), departmentUpdateDto.getDep_id(), "acting_hod");
		int[] reprtViwers = departmentUpdateDto.getReport_viewer();
		int[] reportViwerBefore = departmentUpdateDto.getReport_viewer_before();
		if (updateGroupHodStatus || updateGroupActingHodStatus) {
			for (int i = 0; i < reprtViwers.length; i++) {
				EmployeeGroupDao.updateDataEmployeePermissionTable(reportViwerBefore[i],reprtViwers[i], "report_viwer",
						departmentUpdateDto.getDep_id());
			}
			return true;
		}
		return false;
	}
	
	public void addEmployeetoDepartmentByid(int employeeId, int departmentId) {
		EmployeeGroupDao.insertEmployeeToEmployeeGroupTable(employeeId, departmentId);
	}
	
	public void deleteEmployeetoDepartmentByid(int employeeId, int departmentId) {
		EmployeeGroupDao.deleteEmployeeFromEmployeeGroupTable(employeeId, departmentId);
	}

	private GetAllEmployeeDto getEmployeeDataForDepartmentByEmployeeType(int groupId, String employeeType) {
		GetAllEmployeeDto employee = new GetAllEmployeeDto();
		ResultSet rs = EmployeeGroupDao.getEMployeeDataByGroupId(groupId, employeeType);
		try {
			if (rs.next() == true) {
				employee.setEmployeeid(rs.getInt(1));
				employee.setEmail(rs.getString(2));
				employee.setName(rs.getString(3));
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return employee;
	}

	private ArrayList<GetAllEmployeeDto> getEmployeeListForDepartmentByEmployeeType(int groupId, String employeeType) {
		ArrayList<GetAllEmployeeDto> employeeList = new ArrayList<GetAllEmployeeDto>();
		ResultSet rs = EmployeeGroupDao.getEMployeeDataByGroupId(groupId, employeeType);
		try {
			while (rs.next()) {
				GetAllEmployeeDto employee = new GetAllEmployeeDto();
				employee.setEmployeeid(rs.getInt(1));
				employee.setEmail(rs.getString(2));
				employee.setName(rs.getString(3));
				employeeList.add(employee);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return employeeList;
	}

	private ArrayList<GetAllEmployeeDto> getEmployeeListForDepartmentByEmployeeSystemPermission(int groupId,
			String permisisionType) {
		ArrayList<GetAllEmployeeDto> employeeList = new ArrayList<GetAllEmployeeDto>();
		ResultSet rs = EmployeeGroupDao.getEMployeeDataByEMployeePermission(groupId, permisisionType);
		try {
			while (rs.next()) {
				GetAllEmployeeDto employee = new GetAllEmployeeDto();
				employee.setEmployeeid(rs.getInt(1));
				employee.setEmail(rs.getString(2));
				employee.setName(rs.getString(3));
				employeeList.add(employee);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return employeeList;
	}





}
