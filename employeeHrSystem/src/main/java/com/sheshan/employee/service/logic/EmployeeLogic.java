package com.sheshan.employee.service.logic;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.sheshan.employee.service.dao.EmployeeDao;
import com.sheshan.employee.service.dao.EmployeeGroupDao;
import com.sheshan.employee.service.dto.EmployeeFullProfileDto;
import com.sheshan.employee.service.dto.EmployeeDto;
import com.sheshan.employee.service.dto.EmployeeGoogleProfile;
import com.sheshan.employee.service.dto.EmployeeGroupDto;
import com.sheshan.employee.service.dto.GetAllEmployeeDto;
import com.sheshan.employee.service.dto.LeaveQuotaDto;
import com.sheshan.employee.service.util.ValidateAouth;

public class EmployeeLogic {

	private final String EMAIL_VERIFIED = "true";
	private final String INVALID_TOKEN = "invalid token";
	private final String INVALID_USER = "invalid user";
	private final String EMAIL_ID_MISMATCH = "email id mismatch";
	private final String SYSTEM_ERROR = "system_error";
	private final String EMPLOYEE_ACTIVE_STATUS = "active";
	private final int ACTIVE_STATUS = 1;
	private final String DATE_FORMAT = "yyyy/MM/dd";

	public EmployeeDto validateEmployeeLogin(String tokenId) {
		ValidateAouth googleValidateAouth = new ValidateAouth();
		EmployeeDto employeeDto = new EmployeeDto();
		EmployeeGoogleProfile employeeGoogleProfile = googleValidateAouth.validateIdToken(tokenId);
		if (employeeGoogleProfile.getEmail_verified() != null) {
			if (employeeGoogleProfile.getEmail_verified().toString().equals(EMAIL_VERIFIED)) {
				ResultSet rs = EmployeeDao.getVlaidLoginEmployee(employeeGoogleProfile.getEmail());
				try {
					rs.next();
					if (rs.getInt(1) != 0) {
						employeeDto.setEmployeeId(rs.getInt(1));
						employeeDto.setEmail(rs.getString(2));
						employeeDto.setProfilePicUrl(employeeGoogleProfile.getPicture());
						rs.close();
					} else {
						employeeDto.setError(true);
						employeeDto.setErrorMessage(INVALID_USER);
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					employeeDto.setError(true);
					employeeDto.setErrorMessage(SYSTEM_ERROR);
				}
			} else {
				employeeDto.setError(true);
				employeeDto.setErrorMessage(EMAIL_ID_MISMATCH);
			}
		} else {
			employeeDto.setError(true);
			employeeDto.setErrorMessage(INVALID_TOKEN);
		}
		employeeDto = getEmployeeHodadnGroupDetails(employeeDto);
		ArrayList<LeaveQuotaDto> leaveQuotaList = getEmployeeLeaveQuota(employeeDto);
		employeeDto.setLeaveQuotaList(leaveQuotaList);
		return employeeDto;
	}

	public ArrayList<LeaveQuotaDto> getEmployeeLeaveQuota(EmployeeDto employeeDto) {
		ResultSet rs = EmployeeDao.getEmployeeLeaveQuota(employeeDto.getEmployeeId());
		System.out.println(employeeDto.getEmployeeId());
		ArrayList<LeaveQuotaDto> leaveQuotaList = new ArrayList<>();
		try {
			while (rs.next()) {
				LeaveQuotaDto leaveQuotaObject = new LeaveQuotaDto();
				leaveQuotaObject.setLeaveId(rs.getInt(1));
				leaveQuotaObject.setLeaveType(rs.getString(2));
				leaveQuotaObject.setAnualQuotavValue(rs.getDouble(3));
				leaveQuotaObject.setRemaningQuotaValue(rs.getDouble(4));
				leaveQuotaList.add(leaveQuotaObject);
			}
			rs.close();
			return leaveQuotaList;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			employeeDto.setError(true);
			employeeDto.setErrorMessage(SYSTEM_ERROR);
		}
		return leaveQuotaList;
	}

	public ArrayList<EmployeeDto> getEmployeeListByGroupId(String groupId) {
		ResultSet rs = EmployeeDao.getEmployeeListByGroupId(groupId);

		EmployeeDto employeeDto = new EmployeeDto();

		System.out.println(employeeDto.getEmployeeId());
		ArrayList<EmployeeDto> employeeDtoList = new ArrayList<>();
		try {
			while (rs.next()) {
				employeeDto = new EmployeeDto();
				employeeDto.setEmployeeId(rs.getInt(1));
				employeeDto.setEmail(rs.getString(2));
				ArrayList<EmployeeGroupDto> employeeGroupDtoList = new ArrayList<>();
				EmployeeGroupDto employeeGroupDto = new EmployeeGroupDto();
				employeeGroupDto.setGroupId(rs.getInt(8));
				employeeGroupDto.setName(rs.getString(7));
				employeeGroupDto.setRole(rs.getString(6));
				employeeGroupDtoList.add(employeeGroupDto);
				employeeDto.setGroupList(employeeGroupDtoList);
				employeeDtoList.add(employeeDto);
			}
			rs.close();
			return employeeDtoList;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			employeeDto.setError(true);
			employeeDto.setErrorMessage(SYSTEM_ERROR);
		}
		return employeeDtoList;

	}

	public ArrayList<GetAllEmployeeDto> getAllEmployees(String name, int limit) {
		ResultSet rs = EmployeeDao.getAllEmployee(name, limit);
		ArrayList<GetAllEmployeeDto> getAllEmployeeDto = new ArrayList<>();
		try {
			while (rs.next()) {
				GetAllEmployeeDto newEMployee = new GetAllEmployeeDto();
				newEMployee.setEmployeeid(rs.getInt(1));
				newEMployee.setName(rs.getString(2));
				newEMployee.setEmail(rs.getString(3));
				newEMployee.setDepartment(rs.getString(4));
				newEMployee.setStatus(EMPLOYEE_ACTIVE_STATUS);
				getAllEmployeeDto.add(newEMployee);
			}
			rs.close();
			return getAllEmployeeDto;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return getAllEmployeeDto;

	}

	public EmployeeFullProfileDto getEmployeeFullProfile(int employeeId) {
		ResultSet rs = EmployeeDao.getEmployeefullProfile(employeeId);
		EmployeeFullProfileDto employeeFullProfileDto = new EmployeeFullProfileDto();
		try {
			while (rs.next()) {
				employeeFullProfileDto.setEmployeeid(rs.getInt(1));
				employeeFullProfileDto.setFullname(rs.getString(2));
				employeeFullProfileDto.setEmail(rs.getString(3));
				employeeFullProfileDto.setDob(rs.getString(4));
				employeeFullProfileDto.setGender(rs.getString(5));
				employeeFullProfileDto.setNic_no(rs.getString(6));
				employeeFullProfileDto.setAddress(rs.getString(7));
				employeeFullProfileDto.setEmergency_contact(rs.getString(8));
				employeeFullProfileDto.setCivil_status(rs.getString(9));
				employeeFullProfileDto.setBlood_group(rs.getString(10));
				employeeFullProfileDto.setDesignation(rs.getString(11));
				employeeFullProfileDto.setEpfno(rs.getString(12));
				employeeFullProfileDto.setLocation(rs.getString(13));
				employeeFullProfileDto.setDate_of_join(rs.getString(14));
				employeeFullProfileDto.setProbation_priod(rs.getInt(15));
				employeeFullProfileDto.setDate_of_confimation(rs.getString(16));
				employeeFullProfileDto.setDate_of_resignation(rs.getString(17));
				employeeFullProfileDto.setDepartment(rs.getString(18));
				employeeFullProfileDto.setUser_role(rs.getString(19));
				employeeFullProfileDto.setReporting_manager(rs.getString(20));
				employeeFullProfileDto.setEmp_status(EMPLOYEE_ACTIVE_STATUS);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return employeeFullProfileDto;
	}

	public boolean addEmployee(EmployeeFullProfileDto employeeAddDto) {
		int employeeId;
		java.sql.Date resignationDate;
		try {
			SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
			Date parsed_joinedDate = format.parse(employeeAddDto.getDate_of_join());
			Date parsed_dob = format.parse(employeeAddDto.getDob());
			Date parsed_dateConfirmation = format.parse(employeeAddDto.getDate_of_confimation());
			java.sql.Date joinedDate = new java.sql.Date(parsed_joinedDate.getTime());
			java.sql.Date dob = new java.sql.Date(parsed_dob.getTime());
			java.sql.Date confirmationDate = new java.sql.Date(parsed_dateConfirmation.getTime());
			if (employeeAddDto.getDate_of_resignation().equals("")) {
				resignationDate = null;
			} else {
				Date parsed_dateResignation = format.parse(employeeAddDto.getDate_of_resignation());
				resignationDate = new java.sql.Date(parsed_dateResignation.getTime());
			}
			ResultSet rs = EmployeeDao.insertDatatoEmployeeTable(employeeAddDto.getEmail(),
					employeeAddDto.getEmp_status(), joinedDate, ACTIVE_STATUS, employeeAddDto.getFullname(),
					employeeAddDto.getFullname());
			rs.next();
			employeeId = rs.getInt(1);
			System.out.println("~~~~" + employeeId);
			if (employeeId != 0) {
				boolean insertToEmployeeGroupStatus = EmployeeDao.insertDatatoEMployeeGroupTable(employeeId,
						employeeAddDto.getDepartment(), employeeAddDto.getUser_role());
				if (insertToEmployeeGroupStatus) {
					boolean inserttoEmployeePerosnalTable = EmployeeDao.insertDatatoEmployeePerosnalTable(dob,
							employeeAddDto.getGender(), employeeAddDto.getNic_no(), employeeAddDto.getAddress(),
							employeeAddDto.getEmergency_contact(), employeeAddDto.getCivil_status(),
							employeeAddDto.getBlood_group(), employeeAddDto.getDesignation(), employeeAddDto.getEpfno(),
							joinedDate, confirmationDate, resignationDate, employeeId,
							employeeAddDto.getProbation_priod(), employeeAddDto.getLocation(),
							Integer.parseInt(employeeAddDto.getReporting_manager()));
					if (inserttoEmployeePerosnalTable) {
						return inserttoEmployeePerosnalTable;
					} else {
						EmployeeDao.DeleteFromEmployeeGroupTable(employeeId);
						EmployeeDao.DeleteFromEmployeeTable(employeeId);
						return false;
					}
				} else {
					EmployeeDao.DeleteFromEmployeeTable(employeeId);
					return false;
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public boolean updateEmployee(EmployeeFullProfileDto employeeAddDto) {
		java.sql.Date resignationDate;
		int status = 0;
		try {
			SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
			Date parsed_joinedDate = format.parse(employeeAddDto.getDate_of_join());
			Date parsed_dob = format.parse(employeeAddDto.getDob());
			Date parsed_dateConfirmation = format.parse(employeeAddDto.getDate_of_confimation());
			java.sql.Date joinedDate = new java.sql.Date(parsed_joinedDate.getTime());
			java.sql.Date dob = new java.sql.Date(parsed_dob.getTime());
			java.sql.Date confirmationDate = new java.sql.Date(parsed_dateConfirmation.getTime());
			if (employeeAddDto.getDate_of_resignation().equals("")) {
				resignationDate = null;
			} else {
				Date parsed_dateResignation = format.parse(employeeAddDto.getDate_of_resignation());
				resignationDate = new java.sql.Date(parsed_dateResignation.getTime());
			}
			if(employeeAddDto.getLogin_status().equals("active")){
				status = ACTIVE_STATUS;
			}
			boolean employeeTableUpdateStatus = EmployeeDao.updateEmployeeTable(employeeAddDto.getEmail(),
					employeeAddDto.getEmp_status(), joinedDate, status, employeeAddDto.getFullname(),
					employeeAddDto.getFullname(), employeeAddDto.getEmployeeid());

			if (employeeTableUpdateStatus) {
				boolean updateEmployeeGroupStatus = EmployeeDao.updateDataEmployeeGroupTable(
						employeeAddDto.getEmployeeid(), employeeAddDto.getDepartment(), employeeAddDto.getUser_role());
				if (updateEmployeeGroupStatus) {
					boolean updateEmployeePerosnalTable = EmployeeDao.updateDatatoEmployeePerosnalTable(dob,
							employeeAddDto.getGender(), employeeAddDto.getNic_no(), employeeAddDto.getAddress(),
							employeeAddDto.getEmergency_contact(), employeeAddDto.getCivil_status(),
							employeeAddDto.getBlood_group(), employeeAddDto.getDesignation(), employeeAddDto.getEpfno(),
							joinedDate, confirmationDate, resignationDate, employeeAddDto.getEmployeeid(),
							employeeAddDto.getProbation_priod(), employeeAddDto.getLocation(),
							Integer.parseInt(employeeAddDto.getReporting_manager()));
					return updateEmployeePerosnalTable;
				} else {
					return updateEmployeeGroupStatus;
				}
			}
			return employeeTableUpdateStatus;
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
	}

	private EmployeeDto getEmployeeHodadnGroupDetails(EmployeeDto employeeDto) {
		ResultSet rs = EmployeeGroupDao.getEmployeeGroupHODandGroupName(employeeDto.getEmployeeId());
		System.out.println(employeeDto.getEmployeeId());
		try {
			while (rs.next()) {
				employeeDto.setGroupName(rs.getString(1));
				employeeDto.setHodName(rs.getString(2));
			}
			rs.close();
			return employeeDto;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();		
			employeeDto.setError(true);
			employeeDto.setErrorMessage(SYSTEM_ERROR);
		}
		return employeeDto;

	}

}
