package com.sheshan.employee.service.logic;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import com.sheshan.employee.service.dao.EmployeeDao;
import com.sheshan.employee.service.dao.LeaveDao;
import com.sheshan.employee.service.dto.EmployeeLeaveHistoryDto;
import com.sheshan.employee.service.dto.LeaveConfigByTypeDto;
import com.sheshan.employee.service.dto.LeaveConfigDto;
import com.sheshan.employee.service.dto.LeaveQuotaDto;

public class LeaveLogic {

	public boolean enableDisableLeaveType(String leaveType, int leaveStatus) {
		boolean status = LeaveDao.enableDisableLeaveTypes(leaveType, leaveStatus);
		return status;
	}

	public LeaveConfigByTypeDto getLeaveConfig(String leaveType) {
		// TODO Auto-generated method stub
		ResultSet rs = LeaveDao.getLeaveConfigurationQuotaByLeaveType(leaveType);
		LeaveConfigByTypeDto leaveConfigByDto = new LeaveConfigByTypeDto();
		leaveConfigByDto.setLeave_type(leaveType);
		ArrayList<LeaveConfigDto> leaveConfigDtoList = new ArrayList<>();
		try {
			while (rs.next()) {
				LeaveConfigDto newLeaveConfigDto = new LeaveConfigDto();
				newLeaveConfigDto.setLabel(rs.getString(1));
				newLeaveConfigDto.setValue(rs.getDouble(2));
				leaveConfigDtoList.add(newLeaveConfigDto);
			}
			leaveConfigByDto.setConfig(leaveConfigDtoList);
			return leaveConfigByDto;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return leaveConfigByDto;

	}

	public boolean updateLeaveConfig(LeaveConfigByTypeDto leaveConfigByTypeDto) {
		Iterator<LeaveConfigDto> iterator = leaveConfigByTypeDto.getConfig().iterator();
		try {
			while (iterator.hasNext()) {
				System.out.println();
				LeaveConfigDto leaveConfgDto = iterator.next();
				LeaveDao.updateLeaveConfig(leaveConfigByTypeDto.getLeave_type(), leaveConfgDto.getLabel(),
						leaveConfgDto.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public ArrayList<EmployeeLeaveHistoryDto> getLeaveHistory(String empId) {
		int employeeId = Integer.parseInt(empId);
		System.out.println("Employee ID passed " + employeeId);
		 ArrayList<EmployeeLeaveHistoryDto> employeeLeaveHistpryDtoList= LeaveDao.getEmployeeLeaveHistory(employeeId);
		return employeeLeaveHistpryDtoList;
	}

	public ArrayList<LeaveQuotaDto>getLeaveInventoryForEmployee(String empId) {
		int employeeId = Integer.parseInt(empId);
		System.out.println("Employee ID passed " + employeeId);
		ResultSet rs = EmployeeDao.getEmployeeLeaveQuota(employeeId);
		ArrayList<LeaveQuotaDto> leaveQuotaList = new ArrayList<>();
		try {
			while (rs.next()) {
				LeaveQuotaDto leaveQuotaObject = new LeaveQuotaDto();
				leaveQuotaObject.setLeaveId(rs.getInt(1));
				leaveQuotaObject.setLeaveType(rs.getString(2));
				leaveQuotaObject.setAnualQuotavValue(rs.getDouble(3));
				leaveQuotaObject.setRemaningQuotaValue(rs.getDouble(4));
				leaveQuotaList.add(leaveQuotaObject);
			}
			rs.close();
			return leaveQuotaList;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return leaveQuotaList;
	}

}
