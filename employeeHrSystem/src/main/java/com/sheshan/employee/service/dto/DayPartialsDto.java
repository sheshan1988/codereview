package com.sheshan.employee.service.dto;

public class DayPartialsDto {

	private String work;
	private String start;
	private String end;
	
	


	public String getWork() {
		return work;
	}

	public void setWork(String work) {
		this.work = work;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

}
