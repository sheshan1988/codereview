package com.sheshan.employee.service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.sheshan.employee.service.logic.GoogleSheetAcessor;
import com.sheshan.employee.service.logic.LeaveQuotaCalculationLogic;

public class TestLogic {
	
	 //@Test
	 public void testEmployeeleaveQuota(){
		 DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		 Date date = new Date();
		 LeaveQuotaCalculationLogic newLogic = new LeaveQuotaCalculationLogic();
		 double a = newLogic.generatePermenetLeaveQuota(date);
		 System.out.println(a);
		 
		 
		 try {
			Date date1 = dateFormat.parse("2017/12/31 10:20:20");
			Date date2 = dateFormat.parse("2017/01/28 10:20:20");
			Date date3 = dateFormat.parse("2017/5/16 10:20:20");
			Date date4 = dateFormat.parse("2016/5/16 10:20:20");
			double b = newLogic.generatePermenetLeaveQuota(date1);
			double c = newLogic.generatePermenetLeaveQuota(date2);
			double d = newLogic.generatePermenetLeaveQuota(date3);
			double e = newLogic.generatePermenetLeaveQuota(date4);
			System.out.println(b );
			System.out.println(c );
			System.out.println(d );
			System.out.println(e );
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }
	 
	 @Test
	 public void testSheets(){
		 
	        Sheets service;
			try {
				service = GoogleSheetAcessor.getSheetsService();
		
	        // Prints the names and majors of students in a sample spreadsheet:
	        // https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
			//https://docs.google.com/a/wso2telco.com/spreadsheets/d/1r_9TfO82zCiEf8st18dqEDy673EMoqJq2uO9Kk24h2M/edit?usp=sharing
	        String spreadsheetId = "1r_9TfO82zCiEf8st18dqEDy673EMoqJq2uO9Kk24h2M";
	        String range = "Class Data!A2:E";
	        ValueRange response = service.spreadsheets().values()
	            .get(spreadsheetId, range)
	            .execute();
	        List<List<Object>> values = response.getValues();
	        if (values == null || values.size() == 0) {
	            System.out.println("No data found.");
	        } else {
	          System.out.println("Name, Major");
	          for (List row : values) {
	            // Print columns A and E, which correspond to indices 0 and 4.
	            System.out.printf("%s, %s\n", row.get(0), row.get(4));
	          }
	        }
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		 
	 }

}
